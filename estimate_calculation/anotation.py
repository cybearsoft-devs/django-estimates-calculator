response_RetrieveEstimationResults = {
    "cybear_soft_results": {
        "price": {
            "min": 300,
            "max": 22222500
        },
        "amount_of_days_for_development": 138890,
        "team_size": 1
    },
    "client_results": {
        "price": {
            "min": 150,
            "max": 150
        },
        "amount_of_days_for_development": 1,
        "team_size": 1
    },
    "selected_features": [
        {
            "id": 1,
            "sub_features": [
                {
                    "id": 1,
                    "name": "123",
                    "feature": 1
                },
                {
                    "id": 2,
                    "name": "123123",
                    "feature": 1
                }
            ],
            "name": "1",
            "backend_min": 1,
            "backend_max": 1,
            "frontend_min": 1,
            "frontend_max": 1,
            "IOS_min": 1,
            "IOS_max": 1,
            "android_min": 1,
            "android_max": 1,
            "cross_platform_min": 1,
            "cross_platform_max": 1
        },
        {
            "id": 2,
            "sub_features": [],
            "name": "2",
            "backend_min": 2,
            "backend_max": 2,
            "frontend_min": 2,
            "frontend_max": 2,
            "IOS_min": 2,
            "IOS_max": 2,
            "android_min": 2,
            "android_max": 2,
            "cross_platform_min": 2,
            "cross_platform_max": 2
        }
    ],
    "client_name": "DJOE",
    "email": "some_mail@mail.com"
}