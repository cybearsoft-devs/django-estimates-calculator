import copy
import json

from rest_framework.generics import ListAPIView

from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from django.core.mail import send_mail
from .serializers import FeatureSerializer, TechnologiesSerializer, CustomFeatureSerializer
from .models import Technologies
from .additional_logic import *
from rest_framework import status

HOURS_PER_DAY = 8
DEFAULT_TEAM_SIZE = 1
DEFAULT_SPREADSHEET_NAME = "Sheet1"




class ListOfFeatures(ListAPIView):
    """ Get saved in db features """
    queryset = Feature.objects.all()
    serializer_class = FeatureSerializer


class RetrieveEstimationResults(APIView):
    """ calculate total cost for developing project client onsite and with CybearSoft  """

    @swagger_auto_schema(
        request_body=CustomFeatureSerializer())
    def post(self, request, format=None):
        body = request.data
        client_email = body.pop("client_email") if body.get('client_email') else None
        if not client_email:
            return Response(
                "Please provide email and try one more time",
                status=status.HTTP_400_BAD_REQUEST)

        client_name = body.pop("client_name") if body.get('client_name') else None
        client_selected_and_estimated_dicts = body.pop("features")
        if not client_selected_and_estimated_dicts:
            return Response(
                "Client didn't select any feature, please provide at least one feature to make calculations",
                status=status.HTTP_400_BAD_REQUEST)

        client_rate_dict = {key: value for key, value in body.items() if key.endswith('_rate')}
        client_technologies = {key: value for key, value in body.items() if key.endswith('_technology')}
        cybear_soft_rate = {item['type_of_developing']: item['price'] for item in get_current_rate()}

        copy_client_selected_and_estimated_dicts = copy.deepcopy(client_selected_and_estimated_dicts)
        try:
            client_selected_features_ids = [item.pop('id') for item in client_selected_and_estimated_dicts]
        except KeyError:
            return Response("Please provide id of feature for each selected feature",
                            status=status.HTTP_400_BAD_REQUEST)

        serializer_client_estimate = FeatureSerializer(data=client_selected_and_estimated_dicts, many=True)
        if serializer_client_estimate.is_valid():
            cybear_soft_features_estimation_objects = get_features_by_ids(client_selected_features_ids)

            cybear_soft_serializer = FeatureSerializer(cybear_soft_features_estimation_objects, many=True)
            cybear_soft_features_estimation_dicts = [json.loads(json.dumps(item)) for item in
                                                     cybear_soft_serializer.data]
            cybear_soft_features_estimation_dicts = add_cybearsoft_to_feature_name(
                cybear_soft_features_estimation_dicts)
            copy_cybear_soft_features_estimation_dicts = copy.deepcopy(cybear_soft_features_estimation_dicts)

            cybear_soft_results = calculate_statistic(cybear_soft_features_estimation_dicts.copy(),
                                                      cybear_soft_rate)
            client_results = calculate_statistic(client_selected_and_estimated_dicts.copy(), client_rate_dict)

            link_for_spreadsheet, id_of_google_sheet = create_google_sheet(client_name)

            insert_data_into_spreadsheet(cybear_soft_results, client_results,
                                         copy_client_selected_and_estimated_dicts,
                                         copy_cybear_soft_features_estimation_dicts,
                                         id_of_google_sheet, client_rate_dict, client_technologies

                                         )
            send_mail(
                'Comparison of project development estimates (CyberSoft)',
                f'Hello, {client_name}. Thank you for pay attantion to Cybearsoft .This is link for your google sheet with estimate comparison {link_for_spreadsheet} .'
                f"\n For more information about company please visit ->> https://www.cybearsoft.com/",
                'info@cybearsoft.com',
                [client_email]
            )
            save_interested_user_data(client_name, client_email, link_for_spreadsheet)
            return Response({"cybear_soft_results": cybear_soft_results,
                             "client_results": client_results,
                             "selected_features": copy_client_selected_and_estimated_dicts,
                             "client_name": client_name,
                             "email": client_email})

        else:
            return Response(serializer_client_estimate.errors, status=status.HTTP_400_BAD_REQUEST)


class ListOfTechnologies(ListAPIView):
    queryset = Technologies.objects.all()
    serializer_class = TechnologiesSerializer
